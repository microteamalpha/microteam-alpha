#ifndef GLOBALS_H
#define	GLOBALS_H

#include <xc.h> // include processor files - each processor file is guarded.  
//#include "controller/controller.h"
//#include "mapping/mapping.h"

#define PI          3.1416
#define PI_BY_2     1.5708


#define MAZE_WIDTH 8                         //width of the maze in number cells
#define MAZE_SIZE 64                        //size of the maze in number of cells

extern uint8_t map[MAZE_SIZE];      //array containing the distance values for each cell of the maze
extern uint8_t walls[MAZE_SIZE];    //array containing the wall information for each cell (see mapping.h for documentation)
extern int adcData[8];          //array containing the voltages of the IR-Sensors
extern int distances[8];


typedef struct State {
    float x;     // Position in millimeters in the global coordinate system
    float y;     // Position in millimeters in the global coordinate system
    float thetaDeg; // Orientation measured from the global x-axis towards the global y-axis in degrees
    float thetaRad; // Orientation measured from the global x-axis towards the global y-axis in rad
    float vx;    // Speed in millimeters per second
    float vy;    // Speed in millimeters per second
    float v;     // Overall speed in millimeters per second
    float omega;    // Angular velocity in rad per second
    uint8_t cell;   //Number of the cell the mouse is in (0...63)
    uint8_t orientation;    //which side of the maze the mouse is orientated towards to. (NORTH, WEST, EAST, SOUTH)
    
    float revsPerSecRight;  // Revolutions per second of the right motor
    float revsPerSecLeft;   // Revolutions per second of the left motor
}State;

extern State stateGlobal;



#endif	/* XC_HEADER_TEMPLATE_H */

