#include "encoder.h"

void setupEncoder(){
    
    /**** Setup Encoder 1 ******/
    //Count Error Status Flag set to zero 
    //(I think this flag will be set when a counting error occurs)
    QEI1CONbits.CNTERR = 0;
    //Continue operation in Idle mode
    QEI1CONbits.QEISIDL = 0;
    //Quadrature Encoder Interface enabled (x4 mode)
    //with position counter reset by match(MAXCNT)
    QEI1CONbits.QEIM = 0b111; 
    //Index Pulse does not reset position counter
    QEI1CONbits.POSRES = 0;
    
    MAX1CNT     = ENCODER_MAXCOUNT;
    ENCODER1    = ENCODER_INIT_COUNT;
    
    
    /**** Setup Encoder 2 ******/
    //Count Error Status Flag set to zero 
    //(I think this flag will be set when a counting error occurs)
    QEI2CONbits.CNTERR = 0;
    //Continue operation in Idle mode
    QEI2CONbits.QEISIDL = 0;
    //Quadrature Encoder Interface enabled (x4 mode)
    //with position counter reset by match(MAXCNT)
    QEI2CONbits.QEIM = 0b111; 
    //Index Pulse does not reset position counter
    QEI2CONbits.POSRES = 0;
    
    MAX2CNT     = ENCODER_MAXCOUNT;
    ENCODER2    = ENCODER_INIT_COUNT;
    
}

int readEncoder(){
    //this register is always resetted, we have to take that into consideration
    return POSCNT;
}

int16_t getEncoderDiff(uint16_t encoder) {
    
    int16_t encoderDiff;
    
    if (encoder == ENCODER1) {
        encoderDiff = (ENCODER1-ENCODER_INIT_COUNT)*ENCODER1_DIRECTION;
        ENCODER1 = ENCODER_INIT_COUNT;
    }
    else if (encoder == ENCODER2) {
        encoderDiff = (ENCODER2-ENCODER_INIT_COUNT)*ENCODER2_DIRECTION;
        ENCODER2 = ENCODER_INIT_COUNT;
    }
    
    return encoderDiff;
}


float getRevsPerSecond(uint16_t motor){
    
    static float revsPerSecondSmooth =  0;
    
    float revsPerSecond;
    float revs;
    int16_t encoderDiff;
    
    // Get the number of counts that the encoder did since the last call of this function
    if (motor == MOTOR_LEFT) {
        encoderDiff = getEncoderDiff(ENCODER_LEFT);
    }
    else if (motor == MOTOR_RIGHT){
        encoderDiff = getEncoderDiff(ENCODER_RIGHT);
    }
    
    // Get the number of revolutions that the motor did
    revs = (float)encoderDiff/ENCODER_COUNTS_PER_REV;
    
    // Get revs per second
    revsPerSecond = revs/LOCALIZATION_CYCLETIME_S;
    
    // Smoothen with ENCODER_WINDOW_SIZE
    revsPerSecondSmooth = (1.0 - 1.0/ENCODER_WINDOW_SIZE)*revsPerSecondSmooth + (1.0/ENCODER_WINDOW_SIZE)*revsPerSecond;
    
    return revsPerSecondSmooth;
}




