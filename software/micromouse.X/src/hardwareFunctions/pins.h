#ifndef PINS_H 
#define  PINS_H 
 
#include <xc.h>
 
#define PWM_MOTOR1_FORWARD_EN   PWM1CON1bits.PEN1L  
#define PWM_MOTOR1_BACKWARD_EN  PWM1CON1bits.PEN1H  
#define PWM_MOTOR2_FORWARD_EN   PWM1CON1bits.PEN2L  
#define PWM_MOTOR2_BACKWARD_EN  PWM1CON1bits.PEN2H  
 
#define LED1 LATBbits.LATB4 
#define LED2 LATAbits.LATA4 
 


void setupPins();













#ifdef  __cplusplus 
extern "C" { 
#endif 
 
 
 
 
#ifdef  __cplusplus 
} 
#endif 
 
#endif  /* PINS_H */ 
 