/* 
 * File:   adc.h
 * Author: Marcus
 *
 * Created on May 12, 2018, 9:22 AM
 */
 
#ifndef ADC_H
#define  ADC_H
 
#include <xc.h>
 
/**
 * Use this function to setup the ADC.
 * We use the 4-channel 10bit mode. The sampling will be started automatically 
 * after the last conversion (ASAM)and takes 10*T_CY (SAMC and ADCS). Output
 * format is unsigned integer (FORM). After a conversion an interrupt is generated
 * (Change this for device with DMA).
 */
void setupADC();
 
#ifdef  __cplusplus
extern "C" {
#endif
 
#ifdef  __cplusplus
}
#endif
 
#endif  /* ADC_H */
 
 