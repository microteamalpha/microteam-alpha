#include "dma.h"
#include "pins.h"
#include <p33FJ64MC802.h>

//here the adcData will be hold
unsigned int adcData[8]__attribute__((space(dma)));

void setupDMAChannel0(void)
{
	DMA0CONbits.CHEN 	= 0;	// Disable channel
	DMA0CONbits.SIZE 	= 0;	// Data transfer size (1=byte,0=word)
	DMA0CONbits.DIR		= 0;	// Transfer direction (1=read RAM,write to Periph. 0= read from periph, write to RAM)
	DMA0CONbits.HALF	= 0;	// Early block transfer complete interrupt (1=interrupt on half block transfer,0=int on full block transfer)
	DMA0CONbits.NULLW	= 1;	// Null Data write to peripheral mode (Null data write to peripheral as well as write ro RAM, 0=normal)
	DMA0CONbits.AMODE 	= 0b10;	// DMA channel operating mode	2,3=Peripheral Indirect Addressing mode, 1=Register Indirect without Post-Increment mode, 0=Register Indirect with Post-Increment mode
	DMA0CONbits.MODE	= 0;	// DMA channel opering mode select 0=Continuous, Ping-Pong modes disabled, 2=continuous, ping-pong

	DMA0REQbits.FORCE	= 0;	// Force DMA Transfer (1=single DMA transfer,0=automatic initiated by DMA request)
	DMA0REQbits.IRQSEL	= 0b0001101;	// DMA Peripheral IRQ number select (ADC1)

	DMA0STA = (__builtin_dmaoffset(&(adcData[0]))); // start address of DMA RAM
	DMA0PAD = 0x0300;//(volatile unsigned int) &ADC1BUF0;			// address of peripheral sfr (0x0300)
	DMA0CNT	= 2;	// we have 3 a2d  s/h channels for  measurement

	IFS0bits.DMA0IF 	= 0;	// Clear DMA interrupt
	IEC0bits.DMA0IE 	= 1;	// enable interrupt
	IPC1bits.DMA0IP 	= 5;	// set priority of interrupt

	DMA0CONbits.CHEN 	= 1;	// enable channel
}

void __attribute__((interrupt, auto_psv)) _DMA0Interrupt(void)
{
    //if(ADC1BUF0 >= 1020) LED1 = 1;
	IFS0bits.DMA0IF 		= 0;	// Clear DMA interrupt
    // adcDataReady=1;
};
