#include "uart.h"


void setupUART() {
    
    // Disable UART
    U1MODEbits.UARTEN = 0;
    // Disable IrDA Encoder/Decoder
    U1MODEbits.IREN = 0;
    // Only enable U1TX and U1RX pins
    U1MODEbits.UEN = 0;
    // Do not enable high baud rate
    U1MODEbits.BRGH = 0;
    // Set 8-bit data mode with no parity
    U1MODEbits.PDSEL = 0;
    // Use one stop bit
    U1MODEbits.STSEL = 0;
    
    // Interrupt when transmit buffer becomes empty
    U1STAbits.UTXISEL0 = 0;
    U1STAbits.UTXISEL1 = 0;
    // Interrupt when receive buffer is 3/4 full
    U1STAbits.URXISEL = 0b10;   
    
    
    // Reset Interrupt flags
    IFS0bits.U1TXIF = 0;
    IFS0bits.U1RXIF = 0;
    
    // Enable Interrupts
    IEC0bits.U1TXIE = 1;
    IEC0bits.U1RXIE = 1;
    
    // Set interrupt priorities to 3
    IPC2bits.U1RXIP = 3;
    IPC3bits.U1TXIP = 3;
    
    // Set the baudrate
    U1BRG = UART_BRG_115200;
    
    // Enable UART
    U1MODEbits.UARTEN = 1;
    
    // Enable Transmit
    U1STAbits.UTXEN = 1;
    
    
    
    
}

void sendStringViaUART(char *buffer) {
    // Taken from dspic peripheral library
    
    char * temp_ptr = (char *) buffer;

    /* transmit till NULL character is encountered */

    if(U1MODEbits.PDSEL == 3)        /* check if TX is 8bits or 9bits */
    {
        while(*buffer != '\0') 
        {
            while(U1STAbits.UTXBF); /* wait if the buffer is full */
            U1TXREG = *buffer++;    /* transfer data word to TX reg */
        }
    }
    else
    {
        while(*temp_ptr != '\0')
        {
            while(U1STAbits.UTXBF);  /* wait if the buffer is full */
            U1TXREG = *temp_ptr++;   /* transfer data byte to TX reg */
        }
    }
}


void sendCharViaUART(char data) {
    // Taken from dspic peripheral library
    if(U1MODEbits.PDSEL == 3)
        U1TXREG = data;
    else
        U1TXREG = data & 0xFF;
}

void __attribute__((__interrupt__, auto_psv))  _U1TXInterrupt() {
    
    // Reset interrupt flag
    IFS0bits.U1TXIF = 0;
    
}