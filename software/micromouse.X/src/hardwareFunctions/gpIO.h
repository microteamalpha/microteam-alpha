#ifndef GPIO_H
#define	GPIO_H

#include <xc.h>
#include <p33FJ64MC802.h>

/**
 * Sets up Pin 16 as external interrupt.
 */
void setupGPIO();

#endif	/* GPIO_H */

