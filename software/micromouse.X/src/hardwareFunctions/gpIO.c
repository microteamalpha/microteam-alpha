#include "gpIO.h"

void setupGPIO() {
    // Interrupt 0 on negative edge
    INTCON2bits.INT0EP = 1;
    
    // Reset external interrupt 0 flag
    IFS0bits.INT0IF = 0;
    
}


void __attribute__((__interrupt__, auto_psv)) _INT0Interrupt() {
    // Reset external interrupt 0 flag
    IFS0bits.INT0IF = 0;
    
    
    /* DO COOL STUFF HERE */
    
}