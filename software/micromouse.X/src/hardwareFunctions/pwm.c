#include "pwm.h"


void setupControlToPWM() {
    
    setupPWMTimeBase(ACTIVE_PWM_PRESCALE, ACTIVE_PWM_PTPER);
    
    setDutyCycle(MOTOR1, 0);
    setDutyCycle(MOTOR2, 0);
    
    // Initially deactivate all PWMs
    PWM_MOTOR1_FORWARD_EN   =  0;
    PWM_MOTOR1_BACKWARD_EN  =  0; 
    
    PWM_MOTOR2_FORWARD_EN   =  0;
    PWM_MOTOR1_BACKWARD_EN  =  0; 
}


void controlValueToPWM(uint16_t motor, int16_t value) {
    
    uint16_t valueIsNegative = (value < 0);
    uint16_t dutyCycle;
    
    dutyCycle = valueIsNegative ? ((float)value)/MIN_CONTROL_VALUE*100 : ((float)value)/MAX_CONTROL_VALUE*100;
    
    //Saturation due to a maximum of 100% PWM
    dutyCycle = (dutyCycle > 100) ? 100 : dutyCycle;
    
    // Only activate the PWM channel for the desired direction
    if (motor == MOTOR1) {
        if (valueIsNegative) { // Activate Backward PWM Channel
            PWM_MOTOR1_FORWARD_EN = 0;
            PWM_MOTOR1_BACKWARD_EN = 1;
        }
        else { // Activate Forward PWM Channel
            PWM_MOTOR1_BACKWARD_EN = 0;
            PWM_MOTOR1_FORWARD_EN = 1;
        }
        
    } else if (motor == MOTOR2) {
        if (valueIsNegative) { // Activate Backward PWM Channel
            PWM_MOTOR2_FORWARD_EN = 0;
            PWM_MOTOR2_BACKWARD_EN = 1;
        }
        else { // Activate Forward PWM Channel
            PWM_MOTOR2_BACKWARD_EN = 0;
            PWM_MOTOR2_FORWARD_EN = 1;
        }
    }
    
    setDutyCycle(motor, dutyCycle);
}

void setupPWMTimeBase(uint16_t prescale, uint16_t ptper){  
    
    // Disable PWM time base
    P1TCONbits.PTEN = 0;
      
    //PWM time base runs in CPU idle mode (0 for halt) 
    P1TCONbits.PTSIDL = 1;
    //PWM time base operates in free running mode
    P1TCONbits.PTMOD = 0b00;
    //PWM time base postscale = 1:1
    P1TCONbits.PTOPS = 0b0000;
    
    //PWM I/O pin pair is in the Complementary Output mode (1 or independent output mode)
    PWM1CON1bits.PMOD1 = 0;

    //PWM time base prescale 1:1
    P1TCONbits.PTCKPS= prescale;
    
    // Set the PWM period
    P1TPER = ptper;
             
    //Enable PWM time base
    P1TCONbits.PTEN = 1;
    
}


void setDutyCycle(uint16_t motor, uint16_t dutyCycle) {
    
    dutyCycle = (dutyCycle > MAX_PWM_DUTY_CYCLE) ? MAX_PWM_DUTY_CYCLE : dutyCycle;
    
    if (motor == MOTOR1)
        PWM_MOTOR1 = (float)ACTIVE_PWM_PTPER/100 * dutyCycle * 2;   
    else if (motor == MOTOR2)
        PWM_MOTOR2 = (float)ACTIVE_PWM_PTPER/100 * dutyCycle * 2; 
}
