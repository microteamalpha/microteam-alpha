#include "pins.h"
 
 
void setupPins(){ 
    //Pin 1: Master Clear 
    //Pin 2: ADC Sensor 1 
    AD1PCFGLbits.PCFG0 = 0;  
    //Pin 3: ADC Sensor 2 
    AD1PCFGLbits.PCFG1 = 0; 
    //Pin 4: ADC Sensor 3 
    AD1PCFGLbits.PCFG2 = 0; 
    //Pin 5: UART RX
    RPINR18bits.U1RXR = 0b00001; // Map RP1 to U1RX
    //Pin 6: UART TX
    RPOR1bits.RP2R = 0b00011;    // Map RP2 to U1TX
    //Pin 7: Taster 
    //Pin 8: V_SS 
    //Pin 9: CLOCK 1 
    //Pin 10: CLOCK 2 
    //Pin 11: GPIO LED 1 
    TRISBbits.TRISB4 = 0;
    //Pin 12: GPIO LED 2 
    //Pin 13: V_DD 
    //Pin 14: Programmier Clock 
    //Pin 15: Programmier Data 
    //Pin 16: GPIO Motor Enable  
    //Pin 17: Encoder 2 QEA 
    PWM2CON1bits.PEN1H = 0;         //disable PWM 
    RPINR16bits.QEA2R = 0b01000;    //map RP8 to QEA of Encoder 2 
    //Pin 18: Encoder 2 QEB 
    PWM2CON1bits.PEN1L = 0;         //disable PWM 
    RPINR16bits.QEB2R = 0b01001;    //map RP9 to QEB of Encoder 2 
    //Pin 19: V_SS 
    //Pin 20: V_Cap 
    //Pin 21: Encoder 1 QEA 
    PWM1CON1bits.PEN3H = 0;         //disable PWM 
    RPINR14bits.QEA1R = 0b01010;    //map RP10 to QEA of Encoder 1 
    //Pin 22: Encoder 1 QEB 
    PWM1CON1bits.PEN3L = 0;         //disable PWM 
    RPINR14bits.QEB1R = 0b01011;    //map RP11 to QEB of Encoder 1 
    //Pin 23-26 PWM Motors 
    PWM_MOTOR1_FORWARD_EN = 0;
    TRISBbits.TRISB15 = 0;          //config as output
    LATBbits.LATB15 = 0;
    PWM_MOTOR1_BACKWARD_EN = 0;     //set to 0 for init, reset by motor control
    TRISBbits.TRISB14 = 0;          //config as output
    LATBbits.LATB14 = 0;
    PWM_MOTOR2_FORWARD_EN = 0;
    TRISBbits.TRISB13 = 0;          //config as output
    LATBbits.LATB13 = 0;
    PWM_MOTOR2_BACKWARD_EN = 0; 
    TRISBbits.TRISB12 = 0;          //config as output
    LATBbits.LATB12 = 0;
    
    //Pin 27: AV_SS 
    //Pin 28: AV_DD 
     
} 