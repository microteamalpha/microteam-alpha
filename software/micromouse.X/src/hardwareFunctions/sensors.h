#ifndef SENSORS_H
#define	SENSORS_H

#include <xc.h>
#include <math.h>   //for acos
#include "../globals.h"


//those defines need to be set properly to the corresponding field in the adcData array
#define VOLTAGE_SENSOR_FRONT adcData[1]
#define VOLTAGE_SENSOR_LEFT adcData[2]
#define VOLTAGE_SENSOR_RIGHT adcData[3]
#define V_TRESHOLD 1

//the distances measured by the sensors. They correspond to the values in adcData
#define DISTANCE_FRONT distances[1]
#define DISTANCE_LEFT distances[2]
#define DISTANCE_RIGHT distances[3]


#define CELL_WIDTH 180  //cell width in mm
#define CELL_WIDTH_WITHOUT_WALLS 164 
#define D_MIDDLE_TO_SIDE 10 //distance from mouse middle to side sensors in mm
#define HALF_WALL_STRENGTH 6 //in mm



/**
 * This function converts sensor voltages into distance values in mm
 * @param sensorVoltage the voltage of to be converted
 * @return the distance corresponding to the input voltage
 */
int getDistance(int sensorVoltage);


#endif	/* SENSORS_H */

