#ifndef TIMER_H
#define	TIMER_H

#include <xc.h>  
#include <p33FJ64MC802.h>
#include "../main.h"
    
#define TIMER1_CYCLETIME_S      0.01
#define TIMER1_CYCLETIME_MS     10
    
// PR and Prescale for T_timer = 50ms
#define TIMER1_PERIOD_50MS 31249
#define TIMER1_PRESCALE_50MS 0b10

// PR and Prescale for T_timer = 10ms
#define TIMER1_PERIOD_10MS 49999
#define TIMER1_PRESCALE_10MS 0b01

// PR and Prescale for T_timer = 1ms
#define TIMER1_PERIOD_1MS 39999
#define TIMER1_PRESCALE_1MS 0b00
    
    

/**
 * Setup Timer 1 and Timer 1 Interrupt. 
 * The timer has to be started explicitly using the function startTimer1().
 * @param prescale\n
 * Prescale the Timer input clock\n
 * 00: Timer time base = TCY \n
 * 01: Timer time base = 8*TCY \n
 * 10: Timer time base = 64*TCY \n
 * 11: Timer time base = 256*TCY \n
 * @param period
 * Setup the timer period \n
 * period = T_timer/(TCY*prescale) - 1
 * period is 16 bit wide -> 0 ... 65535
 */
void setupTimer1(uint16_t prescale, uint16_t period);

/**
 * Start Timer 1.
 */
void startTimer1();

/**
 * Stop and reset Timer 1.
 */
void stopTimer1();
    

#endif	/* XC_HEADER_TEMPLATE_H */

