#ifndef HARDWARECONTROLLER_H
#define	HARDWARECONTROLLER_H

#include <stdlib.h>
#include "../controller/controller.h"
#include "gpIO.h"
#include "pins.h"

// Define the duty cycle registers for the motors
#define PWM_MOTOR1 P1DC1
#define PWM_MOTOR2 P1DC2

// Define motor values
#define MOTOR1 1
#define MOTOR2 2

// Map motors
#define MOTOR_LEFT  MOTOR1
#define MOTOR_RIGHT MOTOR2

// Define maximum PWM duty cycle in percent
#define MAX_PWM_DUTY_CYCLE 50

// Map directions to values
#define FORWARDS    1
#define BACKWARDS   0


// PTPER and Prescale for T_PWM = 50ms
#define PWM_PTPER_50MS 31249
#define PWM_PRESCALE_50MS 0b11

// PTPER and Prescale for T_PWM = 10ms
#define PWM_PTPER_10MS 24999
#define PWM_PRESCALE_10MS 0b10

// PTPER and Prescale for T_PWM = 1ms
#define PWM_PTPER_1MS 9999
#define PWM_PRESCALE_1MS 0b01

// Define the Prescale and PTPER to be used
#define ACTIVE_PWM_PRESCALE    PWM_PRESCALE_1MS
#define ACTIVE_PWM_PTPER       PWM_PTPER_1MS

/**
 * This function sets up the peripherals connected to the motors.\n
 * The setup can be modified inside the function.
 */
void setupControlToPWM();

/**
 * Use this function to setup the time base for PWM1.
 * The initial duty cycle is set to 0%.
 * @param prescale \n
 * Prescale the PWM input clock\n
 * 00: PWM time base = TCY \n
 * 01: PWM time base = 4*TCY \n
 * 10: PWM time base = 16*TCY \n
 * 11: PWM time base = 64*TCY \n
 * @param ptper \n
 * Setup the PWM period \n
 * PTPER = T_PWM/(TCY*prescale) - 1 \n
 * PTPER is 15 bit wide -> 0 ... 32767 \n
 */
void setupPWMTimeBase(uint16_t prescale, uint16_t ptper);

/**
 * Set the DutyCycle for the Channels of PWM1.
 * 
 * @param motor\n
 * The motor to control. Can be MOTOR_LEFT or MOTOR_RIGHT.
 * 
 * @param dutyCycle
 * Possible values 0..100
 */
void setDutyCycle(uint16_t motor, uint16_t dutyCycle);



/**
 * This function takes the motor to be controlled and the desired
 * control value as inputs and sets the corresponding PWM accordingly.
 * @param motor\n
 * The motor to control. Can be MOTOR_LEFT or MOTOR_RIGHT.
 * @param value\n
 * The desired control value. Can be in the range of MIN_CONTROL_VALUE 
 * ... MAX_CONTROL_VALUE.
 */
void controlValueToPWM(uint16_t motor, int16_t value);

#endif	/* XC_HEADER_TEMPLATE_H */

