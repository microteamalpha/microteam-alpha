#include "adc.h"
#include "pins.h" 
#include <p33FJ64MC802.h>

void setupADC(){
    
    AD1CON1bits.ADON=0; // clear on-bit for configuration
    AD1CON1bits.ADSIDL=0;//no sampling in idle mode
    AD1CON1bits.ADDMABM=1; //DMA channels are written in order of conversion
    AD1CON1bits.AD12B=0;// 10-bit operation
    AD1CON1bits.FORM=0b00; //format is unsigned integer
    AD1CON1bits.SSRC=0b111; //internal counter ends sampling and starts conversion
    AD1CON1bits.SIMSAM=1; // simultaneous sampling in 10 bit mode
    AD1CON1bits.ASAM=1; //sampling starts immediately after last conversion
    
    AD1CON2bits.VCFG=0b000; //select Vref+ and Vref- as internal reference voltage
    AD1CON2bits.CSCNA=0; //disable analog input SCAN on channel 0
    AD1CON2bits.CHPS=0b11; // convert channels CH0-CH3
    
    AD1CON2bits.SMPI=0b0010;//Selects Increment Rate for DMA Address bits or number of sample/conversion operations per interrupt
                       //we set two because we use three s/h channels
    AD1CON2bits.BUFM=0; //always fill buffer starting at address 0x00
    AD1CON2bits.ALTS=0; //always use channel A and do not alternate
    
    
    AD1CON3bits.SAMC = 0b01010; //end sampling after 10 T_AD
    AD1CON3bits.ADCS = 0x00;    //T_AD = 1*T_CY
    
    AD1CON4bits.DMABL=0b000;//one buffer location per analog input
 
    //turn on ADC
    AD1CON1bits.ADON = 1;
    
    //reset interrupt flag bit
    IFS0bits.AD1IF = 0;
    // Disable the interrupt for AD conversion (We use the DMA interrupt))
    IEC0bits.AD1IE = 0;
    // Set interrupt priority for AD conversion interrupt
    IPC3bits.AD1IP = 4;  
}

//void __attribute__((interrupt, auto_psv)) _ADC1Interrupt(void)
//{
    //if(ADC1BUF0 >= 1000) LED1 = 1;
	//IFS0bits.AD1IF = 0;	// Clear DMA interrupt
    // adcDataReady=1;
//};