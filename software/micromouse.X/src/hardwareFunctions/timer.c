#include "timer.h"


void setupTimer1(uint16_t prescale, uint16_t period){
    
    // Reset timer 1 control register
    T1CON = 0;
       
    // Set internal clock as source
    T1CONbits.TCS = 0;
    
    // Disable gated time accumulation
    T1CONbits.TGATE = 0;
   
    // Reset timer 1 to zero
    TMR1 = 0;
    
    // Set prescale
    T1CONbits.TCKPS = prescale;
    
    // Set the period
    PR1 = period;
    
    
    // Reset the interrupt flag for timer 1
    IFS0bits.T1IF = 0;
    
    // Enable the interrupt for timer 1
    IEC0bits.T1IE = 1;
    
    // Set interrupt priority for timer 1
    IPC0bits.T1IP = 4;
    
    
}

void startTimer1() {
    T1CONbits.TON = 1;
}

void stopTimer1() {
    T1CONbits.TON = 0;
    
    // Reset timer 1 to zero
    TMR1 = 0;
}

void __attribute__((__interrupt__, auto_psv)) _T1Interrupt() {
    
    // Reset the interrupt flag for timer 1
    IFS0bits.T1IF = 0;
    
    runAllFunctions();
}