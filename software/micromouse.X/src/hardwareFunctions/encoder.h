#ifndef ENCODER_H
#define ENCODER_H

#include <stdint.h>
#include "timer.h"
#include "pwm.h"
#include "../globals.h"
#include "../localization/localization.h"


#define ENCODER1        POS1CNT
#define ENCODER2        POS2CNT

// These defines are used to set up the correct count direction for the encoders
#define ENCODER1_DIRECTION  1
#define ENCODER2_DIRECTION  1

#define ENCODER_LEFT    ENCODER1
#define ENCODER_RIGHT   ENCODER2

#define ENCODER_MAXCOUNT    65534
#define ENCODER_INIT_COUNT  32767

// The motor encoder has 16 lines per revolution and the muC counts 4 flanks per line
#define ENCODER_COUNTS_PER_REV 64

// The window size used to smoothen the angularVelocity
#define ENCODER_WINDOW_SIZE 5

 
/**
 * Use this function to setup the encoder.
 * We use the x4 mode to get a high resolution. This means the encoder counts
 * all edges on both channels QEA and QEB. The position counter POSCNT is reset 
 * when the value matches MAXCNT (change mode with register QEIM). 
 * MAXCNT is set to ENCODER_MAXCOUNT for both encoders.
 * The counters themselves are initialized to ENCODER_INIT_COUNT. 
 */
void setupEncoder();
 
/**
 * Use this function to read out the register POSCNt
 * @return the value of POSCNT
 */
int readEncoder();

/**
 * This functions returns the difference between the current encoder value and
 * ENCODER_INIT_COUNT. If the function is returning the value with an incorrect
 * sign, modify ENCODER1_DIRECTION and ENCODER2_DIRECTION accordingly. \n
 * The function resets the encoder counter to ENCODER_INIT_COUNT after reading its value.
 * @param encoder \n
 * Can either be ENCODER_LEFT or ENCODER_RIGHT.
 * @return 
 * int16_t: (ENCODER-ENCODER_INIT_COUNT)*ENCODER_DIRECTION
 */
int16_t getEncoderDiff(uint16_t encoder);


/**
 * This function writes the revolutions per second of a motor to the global
 * state variable. \n
 * Exponential smoothing with window size ENCODER_WINDOW_SIZE is used.
 * @param motor \n
 * Can either be MOTOR_LEFT of MOTOR_RIGHT.
 */
float getRevsPerSecond(uint16_t motor);
 
#endif  /* ENCODER_H */
 
 