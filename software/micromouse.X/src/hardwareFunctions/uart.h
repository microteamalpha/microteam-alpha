#ifndef UART_H
#define	UART_H

#include <xc.h> 
#include <p33FJ64MC802.h>
#include "../globals.h"
#include "pins.h"

// Define BRG for different baudrates
#define UART_BRG_9600B 259
#define UART_BRG_2500KB 0
#define UART_BRG_115200 21

void setupUART();
void sendStringViaUART(char *buffer);
void sendCharViaUART(char a);

#endif	/* UART_H */

