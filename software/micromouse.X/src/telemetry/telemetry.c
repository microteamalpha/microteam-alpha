#include "telemetry.h"


void generateTelemetryString(char *buffer, char messageType) {
    
    // Generate json string here and write it to the buffer. Don't forget to 
    // null terminate it
    
    //In this variant we plain create a string consisting of values.
    //This saves in contrast to json overhead in form of key strings.
    //numbers get converted to strings
    //float in the form %16.8e so it is at most 16 char long
    //uint16_t in the form %8d so it is at most 8 char long
    //header: there are different types of messages encoded in a starting char
    //therefore look into the header
    
//    char *ptr;
//    ptr = buffer;
//    if(messageType == CONTROL_MESSAGE)
//    {
//        //header
//        *ptr = CONTROL_MESSAGE;
//        ptr++;
//        
//        //Positions
//        //x
//        sprintf(ptr, "%16.8e", stateGlobal.x);
//        ptr += 16;
//        //y
//        sprintf(ptr, "%16.8e", stateGlobal.y);
//        ptr += 16;
//        //thetaDeg
//        sprintf(ptr, "%16.8e", stateGlobal.thetaDeg);
//        ptr += 16;
//        
//        //control error
//        sprintf(ptr, "%16.8e", controlError.forward);
//        ptr += 16;
//        sprintf(ptr, "%16.8e", controlError.sideward);
//        ptr += 16;
//        sprintf(ptr, "%16.8e", controlError.theta);
//        ptr += 16;
//        
//        //control command
//        sprintf(ptr, "%16.8e", controlCommand.motor_left);
//        ptr += 16;
//        sprintf(ptr, "%16.8e", controlCommand.motor_right);
//        ptr += 16;
//        
//        //just for analysis
//        int bit_length;
//        bit_length = ptr - buffer;
//        
//    
//    }
//    
//    
//    
//    
//    
//    if(messageType == CONTROL_MESSAGE)
//    {
//        buffer[0] = CONTROL_MESSAGE;
//        buffer[1] = 'A';
//        buffer[2] = 'A';
//        buffer[3] = 0x04; // End of Transmission
//        buffer[3] = '\0';
//    
//    }
    
}
