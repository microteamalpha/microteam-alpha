#ifndef TELEMETRY_H
#define	TELEMETRY_H

#include <xc.h> 
#include <string.h>
#include <stdio.h>
#include "../globals.h"


#define TELEMETRY_BUFFER_SIZE 1024

#define CONTROL_MESSAGE 'c'
#define TEST_MESSAGE 'a'


/**
 * This function parses the global state into a json string and writes it to 
 * the buffer given as input.
 * @param buffer \n
 * The buffer to write the json string to.
 */
void generateTelemetryString(char *buffer, char messageType);



#endif	/* XC_HEADER_TEMPLATE_H */

