#include <xc.h>
#include <p33FJ64MC802.h>
#include "globals.h"
#include "hardwareFunctions/dsPIC33config.h"
#include "hardwareFunctions/timer.h"
#include "hardwareFunctions/gpIO.h"
#include "hardwareFunctions/pwm.h"
#include "hardwareFunctions/encoder.h"
#include "hardwareFunctions/adc.h"
#include "hardwareFunctions/pins.h"
#include "hardwareFunctions/sensors.h"
#include "hardwareFunctions/dma.h"
#include "hardwareFunctions/uart.h"
#include "localization/localization.h"
#include "controller/controller.h"
#include "mapping/mapping.h"

// Defines for instruction clock and frequency
#define TCY  25e-9
#define MIPS 40e6

void setupPPL();

State stateGlobal = { .x                  = INITIAL_POSITION_X,
                .y                  = INITIAL_POSITION_Y,
                .thetaDeg           = INITIAL_ORIENTATION_DEG,
                .thetaRad           = INITIAL_ORIENTATION_RAD,
                .vx                 = 0,
                .vy                 = 0,
                .v                  = 0,
                .omega              = 0,
                .revsPerSecLeft     = 0,
                .revsPerSecRight    = 0,
                .cell               = 0,
                .orientation        = NORTH
};

uint8_t map[MAZE_SIZE];
uint8_t walls[MAZE_SIZE];

int distances[8];


int main(void) {
    
    setupPPL();
    setupPins();
    setupControlToPWM();  
    setupUART();
    setupEncoder();    
    setupTimer1(TIMER1_PRESCALE_10MS, TIMER1_PERIOD_10MS);
    setupDMAChannel0();
    setupADC();
    setupMap(walls);
    
    startTimer1();
    
    while(1) {
        /* The actual functionality can be found in runAllFunctions() and is 
         * called periodically in the timer 1 interrupt routine */
    }
    
    return 0;//we never return
}


void runAllFunctions() {
    
    static State    stateGoal;
    static uint16_t actionType;
    
    float revsPerSecLeft;
    float revsPerSecRight;
    
    // Get motor revs from encoders
    revsPerSecLeft = getRevsPerSecond(MOTOR_LEFT);
    revsPerSecRight = getRevsPerSecond(MOTOR_RIGHT);
    
    /* TODO: GET DISTANCES HERE */
    
    
    // Localization with sensor fusion
    stateGlobal = doLocalization(stateGlobal, revsPerSecLeft, revsPerSecRight);
    
    if(goalStateReached(stateGlobal, stateGoal)){
        /* TODO: DO MAPPING HERE -> doMapping(...) */
        updateWallMap();      // ???
        floodMap(map, walls); // ???
        
        
        stateGoal = getNewGoalState(stateGlobal, &map[0], &walls[0], &actionType);
    }
    
    doControl(stateGlobal, stateGoal, actionType); 
}


void setupPPL() {
    /*** oscillator setup --------------------------------------------------
    Here we are using PPL for 8MHz to generate 80MHz clock.
    FCY = 0.5 * (8MHz*40/(2*2)) = 40 MIPS
    ---------------------------------------------------------------------***/
    PLLFBD = 38; //set PPL to M=40
    CLKDIVbits.PLLPRE = 0; //N1 = input/2
    CLKDIVbits.PLLPOST = 0; //N2 = output/2
    if (IN_SIMULATION_MODE != 1)
    {
        while (OSCCONbits.LOCK != 1); //Wait for PPL to lock
    }
}