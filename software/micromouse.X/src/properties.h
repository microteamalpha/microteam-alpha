#ifndef PROPERTIES_H
#define	PROPERTIES_H

#include <xc.h> // include processor files - each processor file is guarded.  

// Dimensions in millimeters
#define WHEEL_DIAMETER      40
#define WHEEL_RADIUS        20
#define WHEEL_CIRCUMFERENCE 125.6637        // 2*PI*RADIUS
#define WHEEL_DISTANCE  100

#define MOUSE_WIDTH     100
#define MOUSE_LENGTH    100




#endif	/* XC_HEADER_TEMPLATE_H */

