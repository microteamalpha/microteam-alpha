#ifndef LOCALIZATION_H
#define	LOCALIZATION_H

#include <xc.h> 
#include <math.h>
#include <stdlib.h>
#include "../globals.h"
#include "../properties.h"
#include "../hardwareFunctions/timer.h"
#include "../hardwareFunctions/sensors.h"

#define INITIAL_POSITION_X          0
#define INITIAL_POSITION_Y          0
#define INITIAL_ORIENTATION_DEG     90
#define INITIAL_ORIENTATION_RAD     PI_BY_2

#define TRANSMISSION_RATIO  33

#define LOCALIZATION_CYCLETIME_S TIMER1_CYCLETIME_S

#define NORTH 0
#define WEST 1
#define EAST 2
#define SOUTH 3

/**
 * This function calculates a position estimate based on the encoder values.
 * The results are stored in the global state variable.
 */
State doLocalization(State stateGlobal, float revsPerSecLeft, float revsPerSecRight);


State getKinematicStateEstimation(State state, float revsPerSecLeft, float revsPerSecRight);

State getIRStateEstimation();

State doSensorFusion(State stateEstimateFilter, State stateEstimateEncoder, State stateEstimateIR);


#endif	/* XC_HEADER_TEMPLATE_H */

