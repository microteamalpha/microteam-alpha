#include "localization.h"

State doLocalization(State stateGlobal, float revsPerSecLeft, float revsPerSecRight) {
    
    State stateEstimateFilter;
    State stateEstimateEncoder;
    State stateEstimateIR;
    
    stateEstimateFilter = getKinematicStateEstimation(stateGlobal, stateGlobal.revsPerSecLeft, stateGlobal.revsPerSecRight);
    stateEstimateEncoder = getKinematicStateEstimation(stateGlobal, revsPerSecLeft, revsPerSecRight);
    stateEstimateIR = getIRStateEstimation(stateEstimateFilter);
    
    return doSensorFusion(stateEstimateFilter, stateEstimateEncoder, stateEstimateIR);
}

State getKinematicStateEstimation(State state, float revsPerSecLeft, float revsPerSecRight) {
    
    float vLeft, vRight;
    
    state.revsPerSecLeft = revsPerSecLeft;
    state.revsPerSecRight = revsPerSecRight;
    
    vLeft = state.revsPerSecLeft/TRANSMISSION_RATIO*2*PI*WHEEL_RADIUS;
    vRight = state.revsPerSecRight/TRANSMISSION_RATIO*2*PI*WHEEL_RADIUS;
    
    state.v = (vLeft+vRight)/2;
    
    state.omega     = (vRight-vLeft)/(0.5*WHEEL_DISTANCE);
    state.thetaRad  += state.omega * LOCALIZATION_CYCLETIME_S;
    
    // Correction of theta  if not between 0 and 2*PI
    state.thetaRad = fmod(state.thetaRad, 2*PI);
    
    state.thetaDeg = state.thetaRad*180/PI;
    
    
    state.vx = cosf(state.thetaRad)*state.v;
    state.vy = sinf(state.thetaRad)*state.v;
    
    state.x += state.vx * LOCALIZATION_CYCLETIME_S;
    state.y += state.vy * LOCALIZATION_CYCLETIME_S;
    
    state.cell = state.y/180*MAZE_WIDTH + state.x/180;
    
    if(state.thetaDeg >=45 && state.thetaDeg <135) state.orientation = NORTH; 
    if(state.thetaDeg >=135 && state.thetaDeg <225) state.orientation = WEST; 
    if(state.thetaDeg >=225 && state.thetaDeg <315) state.orientation = SOUTH; 
    if((state.thetaDeg>=0 && state.thetaDeg<45) || (state.thetaDeg>=315 && state.thetaDeg<360)) state.orientation = EAST;
    
    
    return state;
}
        

State getIRStateEstimation(State kinematic_est_state){
    //if the values are not set by the function, they will remain -1.
    int sensor_x = -1, sensor_y = -1, sensor_theta = -1;
    
    float phi;              //angle between mouse and the middle line parallel to the walls.
                            //if there are two walls to the side phi is estimated using the sensors else it is calculated 
                            //from kinematic_est_state.thetaDeg
    
    State ir_est_state;
    
    if(VOLTAGE_SENSOR_LEFT > V_TRESHOLD && VOLTAGE_SENSOR_RIGHT > V_TRESHOLD){
        
        int adj_l, adj_r;       //adjacent of the measurement triangle
        int a_l, a_r;           //auxiliary  variables to store coordinate estimates (depending on orientation either x or y)
        
        float theta1, theta2;   //for each configuration two thetas are possible, the one closer to the encoder guess is chosen in the end
        
        phi = acos(CELL_WIDTH_WITHOUT_WALLS/(DISTANCE_LEFT+DISTANCE_RIGHT+2*D_MIDDLE_TO_SIDE));
        adj_l = cos(phi)*(DISTANCE_LEFT+D_MIDDLE_TO_SIDE);
        adj_r = cos(phi)*(DISTANCE_RIGHT+D_MIDDLE_TO_SIDE);
        
        switch(kinematic_est_state.orientation){
            case NORTH:
                /*in this case a_l is the x derived from the distance measured with the left sensor and the position of the
                 * wall to the west. a_r is the x derived from the distance measured with the right sensor and the position
                 * of the wall to the right.
                 */
                a_l = (kinematic_est_state.cell%MAZE_WIDTH)*CELL_WIDTH + HALF_WALL_STRENGTH + adj_l;
                a_r = (kinematic_est_state.cell%MAZE_WIDTH + 1)*CELL_WIDTH - HALF_WALL_STRENGTH - adj_r;
                sensor_x = (a_l+a_r) / 2;
                
                theta1 = 90+phi;
                theta2 = 90-phi;
                break;
            case WEST: 
                a_l = (kinematic_est_state.cell/MAZE_WIDTH)*CELL_WIDTH + HALF_WALL_STRENGTH + adj_l;
                a_r = (kinematic_est_state.cell/MAZE_WIDTH +1)*CELL_WIDTH + HALF_WALL_STRENGTH + adj_r;
                sensor_y = (a_l+a_r) / 2;
                
                theta1 = 180-phi;
                theta2 = 180+phi;
                break;
            case EAST:
                a_l = (kinematic_est_state.cell/MAZE_WIDTH +1)*CELL_WIDTH + HALF_WALL_STRENGTH + adj_l;
                a_r = (kinematic_est_state.cell/MAZE_WIDTH)*CELL_WIDTH + HALF_WALL_STRENGTH + adj_r;
                sensor_y = (a_l+a_r) / 2;
                
                theta1 = 360-phi;
                theta2 = phi;
                break;
            case SOUTH: 
                a_l = (kinematic_est_state.cell%MAZE_WIDTH + 1)*CELL_WIDTH - HALF_WALL_STRENGTH - adj_l;
                a_r = (kinematic_est_state.cell%MAZE_WIDTH)*CELL_WIDTH + HALF_WALL_STRENGTH + adj_r;
                sensor_x = (a_l+a_r) / 2;
                
                theta1 = 270-phi;
                theta2 = 270+phi;
                break;
            default:break;
        } 
        
        //choose the theta closer to the estimation of the encoder
        if(fabsf(kinematic_est_state.thetaDeg-theta1) > fabsf(kinematic_est_state.thetaDeg-theta2)) sensor_theta = theta2;
        else sensor_theta = theta1;
    }
    else{       //if phi is not set through an estimation in the above if clause it must be set here so it can be used below.
        switch(kinematic_est_state.orientation){
            case NORTH:
                phi = fabsf(90-kinematic_est_state.thetaDeg);     //fabsf: abs for float
                break;
            case WEST:
                phi = fabsf(180-kinematic_est_state.thetaDeg);
                break;
            case EAST:
                phi = kinematic_est_state.thetaDeg > 270? 360-kinematic_est_state.thetaDeg: kinematic_est_state.thetaDeg;
            case SOUTH:
                phi = fabsf(270-kinematic_est_state.thetaDeg);
                break;
            default: break;
        }
    }
    
    if(VOLTAGE_SENSOR_FRONT > V_TRESHOLD){
        //phi must be set somehow
        
        int adj_f;        //adjacent of the measurement triangle.
        adj_f = cos(phi)*DISTANCE_FRONT;
        switch(kinematic_est_state.orientation){
            case NORTH:
                sensor_y = (kinematic_est_state.cell/MAZE_WIDTH +1)*CELL_WIDTH - HALF_WALL_STRENGTH - adj_f;
                break;
            case WEST: 
                sensor_x = (kinematic_est_state.cell%MAZE_WIDTH)*CELL_WIDTH + HALF_WALL_STRENGTH + adj_f;
                break;
            case EAST: 
                sensor_x = (kinematic_est_state.cell%MAZE_WIDTH +1)*CELL_WIDTH - HALF_WALL_STRENGTH - adj_f;
                break;
            case SOUTH: 
                sensor_y = (kinematic_est_state.cell/MAZE_WIDTH)*CELL_WIDTH +HALF_WALL_STRENGTH + adj_f;
                break;
            default: break;
        }
    } 
    
    ir_est_state.x = sensor_x;
    ir_est_state.y = sensor_y;
    ir_est_state.thetaDeg = sensor_theta;
    
    return ir_est_state;
}


State doSensorFusion(State stateEstimateFilter, State stateEstimateEncoder, State stateEstimateIR) {
    return stateEstimateEncoder;
}