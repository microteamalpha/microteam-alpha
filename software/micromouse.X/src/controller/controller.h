#ifndef CONTROLLER_H
#define	CONTROLLER_H

#include <xc.h>
#include <math.h>
#include <stdlib.h>
#include "../globals.h"
#include "../mapping/mapping.h"
#include "../hardwareFunctions/pwm.h"

#define CONTROL_METHOD_TRAJECTORY 0
#define CONTROL_METHOD_ERROR_BASED 1

#define CONTROL_METHOD CONTROL_METHOD_ERROR_BASED

#define GOAL_ACCURACY_THRESHOLD_FOR_SIDE 5 //in mm
#define GOAL_ACCURACY_THRESHOLD_THETA 5 //in degree

#define P_FORWARD 1
#define P_SIDEWARD 0.04
#define P_THETA 0.1
#define OUTPUT_MULTIPLIER 1

#define ROTATION_ACTION 0
#define TRANSLATION_ACTION 1

// Set control value range
#define MAX_MOTOR_VOLTAGE 6
#define MIN_CONTROL_VALUE -MAX_MOTOR_VOLTAGE
#define MAX_CONTROL_VALUE  MAX_MOTOR_VOLTAGE


typedef struct ControlError {
    float forward; //Error in forward direction
    float sideward; //Error in sideward direction
    float theta; //Error in sideward direction
}ControlError;
extern ControlError controlError;

typedef struct ControlCommand {
    float motor_left;
    float motor_right;
}ControlCommand;
extern ControlCommand controlCommand;

ControlCommand updateControlCommands(ControlError controlError, uint16_t actionType);
ControlError updateControlError(State stateGlobal, State stateGoal);
uint16_t goalStateReached();
State getNewGoalState(State stateGlobal, uint8_t map[MAZE_SIZE], uint8_t walls[MAZE_SIZE], uint16_t *actionType);
void doControl(State stateGlobal, State stateGoal, uint16_t actionType);

#endif	/* CONTROLLER_H */

