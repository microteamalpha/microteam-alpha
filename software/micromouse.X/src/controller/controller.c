#include "controller.h"

void doControl(State stateGlobal, State stateGoal, uint16_t actionType) {
    
    ControlError controlError;
    ControlCommand controlCommand;
    
    controlError = updateControlError(stateGlobal, stateGoal);
    controlCommand = updateControlCommands(controlError, actionType);
    
    // Send the controlCommands to PWM
    controlValueToPWM(MOTOR_LEFT, controlCommand.motor_left);
    controlValueToPWM(MOTOR_RIGHT, controlCommand.motor_right);
    
    
}

ControlCommand updateControlCommands(ControlError controlError, uint16_t actionType){
    float forward = 0;
    float sideward = 0;
    float theta = 0;
    
    ControlCommand controlCommand = {   .motor_left        = 0,
                                        .motor_right       = 0};
    
    forward = controlError.forward * P_FORWARD * actionType;
    sideward = controlError.sideward * P_SIDEWARD * actionType;
    theta = controlError.theta * P_THETA;
            
    
    // TODO: Output normalization and scaling to min/max control value
    controlCommand.motor_left =  (forward + sideward - theta) * OUTPUT_MULTIPLIER;
    controlCommand.motor_right =  (forward - sideward + theta) * OUTPUT_MULTIPLIER;
    
    return controlCommand;
}

ControlError updateControlError(State stateGlobal, State stateGoal){
    float d_x;
    float d_y;
    
    ControlError controlError = {   .forward        = 0,
                                    .sideward       = 0,
                                    .theta          = 0};
    
    //Error based control
    if(CONTROL_METHOD == CONTROL_METHOD_ERROR_BASED){
        d_x = stateGoal.x - stateGlobal.x;
        d_y = stateGoal.y - stateGlobal.y;
        //Calculate what forward and sidewards means in current context
        if(fabsf(d_y) >= fabsf(d_x)){
            controlError.forward = d_y;
            controlError.sideward = d_x;
        }
        else{
            controlError.forward = d_x;
            controlError.sideward = d_y;
        }
        //TODO: With this implementation the controller tends to perform unfavorable
        //Rotations e.g. the Mouse will turn quite around the whole axis when 355 and 0 degree
        controlError.theta = stateGoal.thetaRad - stateGlobal.thetaRad;
    }
    if(CONTROL_METHOD == CONTROL_METHOD_TRAJECTORY){
        //not implemented yet
    }
    
    return controlError;
}

uint16_t goalStateReached(State stateGlobal, State stateGoal){
    //checks if current goal is reached, based on the global variable controlError
    return (fabsf(stateGoal.x - stateGlobal.x) < GOAL_ACCURACY_THRESHOLD_FOR_SIDE
            && fabsf(stateGoal.y - stateGlobal.y) < GOAL_ACCURACY_THRESHOLD_FOR_SIDE
            && fabsf(stateGoal.thetaRad - stateGlobal.thetaRad) < GOAL_ACCURACY_THRESHOLD_THETA);
        
}

State getNewGoalState(State stateGlobal, uint8_t map[MAZE_SIZE], uint8_t walls[MAZE_SIZE], uint16_t *actionType){
    
    uint8_t current_cell = stateGlobal.cell;
    uint8_t orientation = stateGlobal.orientation;
    
    State stateGoal;
    
    uint8_t current_cell_x_mm = (current_cell%MAZE_WIDTH)*180 +90;       //center coordinates of the current_cell in mm
    uint8_t current_cell_y_mm = (current_cell/MAZE_WIDTH)*180 +90;
    uint8_t direction = findGoalDirection(current_cell, &map[0], &walls[0]);
    *actionType = ROTATION_ACTION; //default is the assumption, the mouse has to turn
    
    switch(direction){
        case NORTH: 
          if(orientation == NORTH){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm + 180;
              stateGoal.thetaDeg = 90;
              stateGoal.thetaRad = PI_BY_2;
              *actionType = TRANSLATION_ACTION;
          }
          else if(orientation == WEST){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 180;
              stateGoal.thetaRad = PI;
          }
          else if(orientation == EAST){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 0;
              stateGoal.thetaRad = 0;
          }
          else if(orientation == SOUTH){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 270;
              stateGoal.thetaRad = PI+PI_BY_2;
          }
          break;
        case WEST:
          if(orientation == NORTH){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 90;
              stateGoal.thetaRad = PI_BY_2;
          }
          else if(orientation == WEST){
              stateGoal.x = current_cell_x_mm-180;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 180;
              stateGoal.thetaRad = PI;
              *actionType = TRANSLATION_ACTION;
          }
          else if(orientation == EAST){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 0;
              stateGoal.thetaRad = 0;
          }
          else if(orientation == SOUTH){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 270;
              stateGoal.thetaRad = PI+PI_BY_2;
          }
          break;
        case EAST:
          if(orientation == NORTH){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 90;
              stateGoal.thetaRad = PI_BY_2;
          }
          else if(orientation == WEST){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 180;
              stateGoal.thetaRad = PI;
          }
          else if(orientation == EAST){
              stateGoal.x = current_cell_x_mm+180;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 0;
              stateGoal.thetaRad = 0;
              *actionType = TRANSLATION_ACTION;
          }
          else if(orientation == SOUTH){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 270;
              stateGoal.thetaRad = PI+PI_BY_2;
          }
          break;
        case SOUTH:
          if(orientation == NORTH){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 90;
              stateGoal.thetaRad = PI_BY_2;
          }
          else if(orientation == WEST){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 180;
              stateGoal.thetaRad = PI;
          }
          else if(orientation == EAST){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm;
              stateGoal.thetaDeg = 0;
              stateGoal.thetaRad = 0;
          }
          else if(orientation == SOUTH){
              stateGoal.x = current_cell_x_mm;
              stateGoal.y = current_cell_y_mm-180;
              stateGoal.thetaDeg = 270;
              stateGoal.thetaRad = PI+PI_BY_2;
              *actionType = TRANSLATION_ACTION;
          }
          break;
        default:
            break;
    }
    
    return stateGoal;
}