#ifndef MAPPING_H
#define	MAPPING_H

#include <xc.h>
#include "../globals.h"
#include "../localization/localization.h"
#include "../hardwareFunctions/sensors.h"

#define push(sp, n) (*((sp)++) = (n))       //push to a stack
#define pop(sp) (*--(sp))                   //pop from a stack
#define STACKLENGTH 30                      //maximal length of the stack



/**
 * This sets up the wall array used by the Flood-Fill Algorithm. All walls inside the maze
 * will be reset and walls around the maze will be set.
 * @param walls is the array containing the wall information. Its size is MAZE_SIZE and different lines and columns
 * are distinguished using MAZE_WIDTH
 * The wall information for each cell in the maze is given by the last 4 bits of the corresponding byte.
 * The 4 MSB are not used. 
 * Bit 0 set: Wall to the North
 * Bit 1 set: Wall to the West
 * Bit 2 set: Wall to the East
 * Bit 3 set: Wall to the South
 */
void setupMap(uint8_t walls[MAZE_SIZE]);

/**
 * This functions floods the map array with new distance values.
 * @param map is an array containing the distance values of each cell measuring the distance between the cell and the 
 * goal in the middle. It should be of the Size MAZE_SIZE.
 * @param walls is an array containing information about the walls of each cell. See the comment of setupMap for more information.
 */
void floodMap(uint8_t map[MAZE_SIZE], uint8_t walls[MAZE_SIZE]);
/**
 * This function gives the direction to be taken to get to the adjoining cell with the lowest distance value.
 * @param current_cell is the number of the current cell (0...MAZE_SIZE-1)
 * @param map is an array containing the distance values for each cell
 * @param walls is an array containing the wall information for each cell
 * @return The function return an 8bit integer which represents the direction to be taken
 * 0: NORTH
 * 1: WEST
 * 2: EAST
 * 3: SOUTH
 */
uint8_t findGoalDirection(uint8_t current_cell, uint8_t map[MAZE_SIZE], uint8_t walls[MAZE_SIZE]);

/**
 * This function is called when the mouse moves into an unknown cell. It uses a voltage treshold
 * and the orientation of the mouse to set walls. While doing this it also sets the corresponding wall
 * in the neighboring cells. This only happens if these cells are part of the maze.
 * Example: The mouse is facing north and the voltages of the front and the left sensor exceed the specified treshold:
 * -> the respective bits for a wall to the north and a wall to the west in walls[current_cell] are set.
 * -> additionally the south wall bit of the cell to the north is set.
 * -> additionally the east wall bit of the cell to the west is set.
 * For the definition of the walls array, state.cell and state.orientation check mapping.h and globals.h
 * would be in the south west corner of the maze.
 */
void updateWallMap();

#endif	/* MAPPING_H */

