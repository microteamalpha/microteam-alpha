#include "mapping.h"

void setupMap(uint8_t walls[MAZE_SIZE]){
    int i;
    for(i = 0; i<MAZE_SIZE; i++){
        if(i>MAZE_SIZE-MAZE_WIDTH-1){   //set walls at the top side of the maze
			walls[i] |= 1<<0;
		}
		if(i<MAZE_WIDTH){               //set walls at the bottom side of the maze
			walls[i] |= 1<<3;
		}
		if(i%MAZE_WIDTH == 0){          //set walls at the left side of the maze
			walls[i] |= 1<<1;
		}
		if((i+1)%MAZE_WIDTH == 0){      //set walls at the right side of the maze
			walls[i] |= 1<<2;
		}
    }
}

void updateWallMap(){ 
    if(stateGlobal.orientation == NORTH){             //robot faces north 
        if(VOLTAGE_SENSOR_FRONT>V_TRESHOLD){    //wall to the north    
            walls[stateGlobal.cell] |= 1 << NORTH;      
            if(stateGlobal.cell>=MAZE_SIZE-MAZE_WIDTH) walls[stateGlobal.cell+MAZE_WIDTH] |= 1 << SOUTH; //southern wall on cell to the north 
        } 
        if(VOLTAGE_SENSOR_LEFT>V_TRESHOLD){     //wall to the west
            walls[stateGlobal.cell] |= 1 << WEST;      
            if(stateGlobal.cell%MAZE_WIDTH != 0) walls[stateGlobal.cell-1] |= 1 << EAST; 
        } 
        if(VOLTAGE_SENSOR_RIGHT>V_TRESHOLD){    //wall to the east
            walls[stateGlobal.cell] |= 1 << EAST;      
            if((stateGlobal.cell+1)%MAZE_WIDTH != 0) walls[stateGlobal.cell+1] |= 1 << WEST; 
        } 
    } 
    else if(stateGlobal.orientation == WEST){         //robot faces west
        if(VOLTAGE_SENSOR_FRONT>V_TRESHOLD){    //wall to the west          
            walls[stateGlobal.cell] |= 1 << WEST;      
            if(stateGlobal.cell%MAZE_WIDTH != 0) walls[stateGlobal.cell-1] |= 1 << EAST; 
        } 
        if(VOLTAGE_SENSOR_LEFT>V_TRESHOLD){     //wall to the south
            walls[stateGlobal.cell] |= 1 << SOUTH;      
            if(stateGlobal.cell>=MAZE_WIDTH) walls[stateGlobal.cell-MAZE_WIDTH] |= 1 << NORTH; 
        } 
        if(VOLTAGE_SENSOR_RIGHT>V_TRESHOLD){    //wall to the north 
            walls[stateGlobal.cell] |= 1 << NORTH;      
            if(stateGlobal.cell>=MAZE_SIZE-MAZE_WIDTH) walls[stateGlobal.cell+MAZE_WIDTH] |= 1 << SOUTH; 
        } 
    } 
    else if(stateGlobal.orientation == EAST){         //robot faces east
        if(VOLTAGE_SENSOR_FRONT>V_TRESHOLD){    //wall to the east
            walls[stateGlobal.cell] |= 1 << EAST;      
            if(stateGlobal.cell%MAZE_WIDTH != 0) walls[stateGlobal.cell-1] |= 1 << EAST; 
        } 
        if(VOLTAGE_SENSOR_LEFT>V_TRESHOLD){     //wall to the north 
            walls[stateGlobal.cell] |= 1 << NORTH;     
            if(stateGlobal.cell<=MAZE_SIZE-MAZE_WIDTH) walls[stateGlobal.cell+MAZE_WIDTH] |= 1 << SOUTH; 
        } 
        if(VOLTAGE_SENSOR_RIGHT>V_TRESHOLD){    //wall to the south 
            walls[stateGlobal.cell] |= 1 << SOUTH;     
            if(stateGlobal.cell>=MAZE_WIDTH) walls[stateGlobal.cell-MAZE_WIDTH] |= 1 << NORTH; 
        } 
    } 
    else if(stateGlobal.orientation == SOUTH){        //robot faces south
        if(VOLTAGE_SENSOR_FRONT>V_TRESHOLD){    //wall to the south 
            walls[stateGlobal.cell] |= 1 << SOUTH;     
            if(stateGlobal.cell>=MAZE_WIDTH) walls[stateGlobal.cell-MAZE_WIDTH] |= 1 << NORTH; 
        } 
        if(VOLTAGE_SENSOR_LEFT>V_TRESHOLD){     //wall to the east  
            walls[stateGlobal.cell] |= 1 << EAST;     
            if((stateGlobal.cell+1)%MAZE_WIDTH != 0) walls[stateGlobal.cell+1] |= 1 << WEST; 
        } 
        if(VOLTAGE_SENSOR_RIGHT>V_TRESHOLD){    //wall to the west 
            walls[stateGlobal.cell] |= 1 << WEST;      
            if(stateGlobal.cell%MAZE_WIDTH != 0) walls[stateGlobal.cell-1] |= 1 << EAST; 
        } 
    }    
} 



void floodMap(uint8_t map[MAZE_SIZE], uint8_t walls[MAZE_SIZE]){
    /*
     * The algorithm uses two stacks. Two pointers current_level and next_level are pointing to the stacks and there values are
     * exchanged after each iteration which means whenever the integer level is increased.
     * The pointer help is used when switching the pointer values.
     * An array visited is used to prevent pushing cells on the stack that are already on it.
     */
	uint8_t stack1[STACKLENGTH];
	uint8_t* current_level;
	current_level = stack1;
    
    uint8_t stack2[STACKLENGTH];
	uint8_t* next_level;
	next_level = stack2;
    
    uint8_t current_level_length = 0;
	uint8_t next_level_length = 0;
	uint8_t done = 0;

	uint8_t* help;
    
	uint8_t visited[MAZE_SIZE];
    int i;
	for(i = 0; i<MAZE_SIZE;i++){
		visited[i] = 0;
	}

    for(i = 0; i<MAZE_SIZE; i++){
        map[i] = MAZE_SIZE;         //cells are initialized with a large distance value
    }
	
	push(current_level, 27);        //push the 4 goal cells on the stack
	push(current_level, 28);
	push(current_level, 35);
	push(current_level, 36);
	visited[27] = 1;
	visited[28] = 1;
	visited[35] = 1;
	visited[36] = 1;
	current_level_length = 4;       //set stack length to 4 (needed for termination later on)

	uint8_t current_cell;

	uint8_t level = 0;
	while(done != 1){
		while(current_level_length>0){
			current_cell = pop(current_level);
			current_level_length--;
			if(map[current_cell] == MAZE_SIZE){
					map[current_cell] = level;
					if(!(walls[current_cell] & 1<<NORTH) && visited[current_cell+MAZE_WIDTH] == 0){				//no wall to the north
						push(next_level, current_cell+MAZE_WIDTH);	//push cell to the north
						visited[current_cell+MAZE_WIDTH] = 1;
						next_level_length++;
					}
					if(!(walls[current_cell] & 1<<WEST) && visited[current_cell-1] == 0){				//no wall to the west
						push(next_level, current_cell-1);	//push cell to the west
						visited[current_cell-1] = 1;
						next_level_length++;
					}
					if(!(walls[current_cell] & 1<<EAST) && visited[current_cell+1] == 0){				//no wall to the east
						push(next_level, current_cell+1);	//push cell to the east
						visited[current_cell+1] = 1;
						next_level_length++;
					}
					if(!(walls[current_cell] & 1<<SOUTH) && visited[current_cell-MAZE_WIDTH] == 0){				//no wall to the south
						push(next_level, current_cell-MAZE_WIDTH);	//push cell to the south
						visited[current_cell-MAZE_WIDTH] = 1;
						next_level_length++;
					}
			}
		}
		if(next_level_length == 0){
			done = 1;
		}
		else{
			level++;
			help = current_level;		//switch the two stacks, the stack current_level was pointing to before is empty
			current_level = next_level;
			next_level = help;
			current_level_length = next_level_length;
			next_level_length = 0;
		}
	}
}

uint8_t findGoalDirection(uint8_t current_cell, uint8_t map[MAZE_SIZE], uint8_t walls[MAZE_SIZE]){
    uint8_t neighbors_distance_values[4] = {64,64,64,64};
	if(!(walls[current_cell] & 1 << NORTH)) neighbors_distance_values[0] = map[current_cell + MAZE_WIDTH];  //if there is no wall to the North the cell will be considered as a possible destination
	if(!(walls[current_cell] & 1 << WEST)) neighbors_distance_values[1] = map[current_cell - 1];
	if(!(walls[current_cell] & 1 << EAST)) neighbors_distance_values[2] = map[current_cell + 1];
	if(!(walls[current_cell] & 1 << SOUTH)) neighbors_distance_values[3] = map[current_cell - MAZE_WIDTH];
	int i;
	int min = 50;
	int ind_min = -1;   
	for(i=0;i<4;i++){                           //find that open neighbor with the lowest distance value
		if(neighbors_distance_values[i]<min){
			min = neighbors_distance_values[i];
			ind_min = i;
		}
	}
	return ind_min;
}

