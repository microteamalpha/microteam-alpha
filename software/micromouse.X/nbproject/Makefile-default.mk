#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/micromouse.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/micromouse.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS
SUB_IMAGE_ADDRESS_COMMAND=--image-address $(SUB_IMAGE_ADDRESS)
else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=src/main.c src/hardwareFunctions/timer.c src/hardwareFunctions/gpIO.c src/hardwareFunctions/pins.c src/hardwareFunctions/pwm.c src/hardwareFunctions/adc.c src/hardwareFunctions/encoder.c src/hardwareFunctions/dma.c src/localization/localization.c src/telemetry/telemetry.c src/controller/controller.c src/hardwareFunctions/sensors.c src/mapping/mapping.c src/hardwareFunctions/uart.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/src/main.o ${OBJECTDIR}/src/hardwareFunctions/timer.o ${OBJECTDIR}/src/hardwareFunctions/gpIO.o ${OBJECTDIR}/src/hardwareFunctions/pins.o ${OBJECTDIR}/src/hardwareFunctions/pwm.o ${OBJECTDIR}/src/hardwareFunctions/adc.o ${OBJECTDIR}/src/hardwareFunctions/encoder.o ${OBJECTDIR}/src/hardwareFunctions/dma.o ${OBJECTDIR}/src/localization/localization.o ${OBJECTDIR}/src/telemetry/telemetry.o ${OBJECTDIR}/src/controller/controller.o ${OBJECTDIR}/src/hardwareFunctions/sensors.o ${OBJECTDIR}/src/mapping/mapping.o ${OBJECTDIR}/src/hardwareFunctions/uart.o
POSSIBLE_DEPFILES=${OBJECTDIR}/src/main.o.d ${OBJECTDIR}/src/hardwareFunctions/timer.o.d ${OBJECTDIR}/src/hardwareFunctions/gpIO.o.d ${OBJECTDIR}/src/hardwareFunctions/pins.o.d ${OBJECTDIR}/src/hardwareFunctions/pwm.o.d ${OBJECTDIR}/src/hardwareFunctions/adc.o.d ${OBJECTDIR}/src/hardwareFunctions/encoder.o.d ${OBJECTDIR}/src/hardwareFunctions/dma.o.d ${OBJECTDIR}/src/localization/localization.o.d ${OBJECTDIR}/src/telemetry/telemetry.o.d ${OBJECTDIR}/src/controller/controller.o.d ${OBJECTDIR}/src/hardwareFunctions/sensors.o.d ${OBJECTDIR}/src/mapping/mapping.o.d ${OBJECTDIR}/src/hardwareFunctions/uart.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/src/main.o ${OBJECTDIR}/src/hardwareFunctions/timer.o ${OBJECTDIR}/src/hardwareFunctions/gpIO.o ${OBJECTDIR}/src/hardwareFunctions/pins.o ${OBJECTDIR}/src/hardwareFunctions/pwm.o ${OBJECTDIR}/src/hardwareFunctions/adc.o ${OBJECTDIR}/src/hardwareFunctions/encoder.o ${OBJECTDIR}/src/hardwareFunctions/dma.o ${OBJECTDIR}/src/localization/localization.o ${OBJECTDIR}/src/telemetry/telemetry.o ${OBJECTDIR}/src/controller/controller.o ${OBJECTDIR}/src/hardwareFunctions/sensors.o ${OBJECTDIR}/src/mapping/mapping.o ${OBJECTDIR}/src/hardwareFunctions/uart.o

# Source Files
SOURCEFILES=src/main.c src/hardwareFunctions/timer.c src/hardwareFunctions/gpIO.c src/hardwareFunctions/pins.c src/hardwareFunctions/pwm.c src/hardwareFunctions/adc.c src/hardwareFunctions/encoder.c src/hardwareFunctions/dma.c src/localization/localization.c src/telemetry/telemetry.c src/controller/controller.c src/hardwareFunctions/sensors.c src/mapping/mapping.c src/hardwareFunctions/uart.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/micromouse.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=33FJ64MC802
MP_LINKER_FILE_OPTION=,--script=p33FJ64MC802.gld
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/src/main.o: src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/main.o.d 
	@${RM} ${OBJECTDIR}/src/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/main.c  -o ${OBJECTDIR}/src/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/main.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/timer.o: src/hardwareFunctions/timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/timer.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/timer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/timer.c  -o ${OBJECTDIR}/src/hardwareFunctions/timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/timer.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/gpIO.o: src/hardwareFunctions/gpIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/gpIO.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/gpIO.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/gpIO.c  -o ${OBJECTDIR}/src/hardwareFunctions/gpIO.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/gpIO.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/gpIO.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/pins.o: src/hardwareFunctions/pins.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/pins.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/pins.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/pins.c  -o ${OBJECTDIR}/src/hardwareFunctions/pins.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/pins.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/pins.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/pwm.o: src/hardwareFunctions/pwm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/pwm.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/pwm.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/pwm.c  -o ${OBJECTDIR}/src/hardwareFunctions/pwm.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/pwm.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/pwm.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/adc.o: src/hardwareFunctions/adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/adc.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/adc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/adc.c  -o ${OBJECTDIR}/src/hardwareFunctions/adc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/adc.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/adc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/encoder.o: src/hardwareFunctions/encoder.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/encoder.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/encoder.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/encoder.c  -o ${OBJECTDIR}/src/hardwareFunctions/encoder.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/encoder.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/encoder.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/dma.o: src/hardwareFunctions/dma.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/dma.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/dma.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/dma.c  -o ${OBJECTDIR}/src/hardwareFunctions/dma.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/dma.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/dma.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/localization/localization.o: src/localization/localization.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/localization" 
	@${RM} ${OBJECTDIR}/src/localization/localization.o.d 
	@${RM} ${OBJECTDIR}/src/localization/localization.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/localization/localization.c  -o ${OBJECTDIR}/src/localization/localization.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/localization/localization.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/localization/localization.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/telemetry/telemetry.o: src/telemetry/telemetry.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/telemetry" 
	@${RM} ${OBJECTDIR}/src/telemetry/telemetry.o.d 
	@${RM} ${OBJECTDIR}/src/telemetry/telemetry.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/telemetry/telemetry.c  -o ${OBJECTDIR}/src/telemetry/telemetry.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/telemetry/telemetry.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/telemetry/telemetry.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/controller/controller.o: src/controller/controller.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/controller" 
	@${RM} ${OBJECTDIR}/src/controller/controller.o.d 
	@${RM} ${OBJECTDIR}/src/controller/controller.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/controller/controller.c  -o ${OBJECTDIR}/src/controller/controller.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/controller/controller.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/controller/controller.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/sensors.o: src/hardwareFunctions/sensors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/sensors.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/sensors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/sensors.c  -o ${OBJECTDIR}/src/hardwareFunctions/sensors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/sensors.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/sensors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/mapping/mapping.o: src/mapping/mapping.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/mapping" 
	@${RM} ${OBJECTDIR}/src/mapping/mapping.o.d 
	@${RM} ${OBJECTDIR}/src/mapping/mapping.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/mapping/mapping.c  -o ${OBJECTDIR}/src/mapping/mapping.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/mapping/mapping.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/mapping/mapping.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/uart.o: src/hardwareFunctions/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/uart.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/uart.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/uart.c  -o ${OBJECTDIR}/src/hardwareFunctions/uart.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/uart.o.d"      -g -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1    -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/uart.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
else
${OBJECTDIR}/src/main.o: src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src" 
	@${RM} ${OBJECTDIR}/src/main.o.d 
	@${RM} ${OBJECTDIR}/src/main.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/main.c  -o ${OBJECTDIR}/src/main.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/main.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/main.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/timer.o: src/hardwareFunctions/timer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/timer.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/timer.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/timer.c  -o ${OBJECTDIR}/src/hardwareFunctions/timer.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/timer.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/timer.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/gpIO.o: src/hardwareFunctions/gpIO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/gpIO.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/gpIO.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/gpIO.c  -o ${OBJECTDIR}/src/hardwareFunctions/gpIO.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/gpIO.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/gpIO.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/pins.o: src/hardwareFunctions/pins.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/pins.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/pins.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/pins.c  -o ${OBJECTDIR}/src/hardwareFunctions/pins.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/pins.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/pins.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/pwm.o: src/hardwareFunctions/pwm.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/pwm.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/pwm.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/pwm.c  -o ${OBJECTDIR}/src/hardwareFunctions/pwm.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/pwm.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/pwm.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/adc.o: src/hardwareFunctions/adc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/adc.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/adc.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/adc.c  -o ${OBJECTDIR}/src/hardwareFunctions/adc.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/adc.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/adc.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/encoder.o: src/hardwareFunctions/encoder.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/encoder.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/encoder.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/encoder.c  -o ${OBJECTDIR}/src/hardwareFunctions/encoder.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/encoder.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/encoder.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/dma.o: src/hardwareFunctions/dma.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/dma.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/dma.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/dma.c  -o ${OBJECTDIR}/src/hardwareFunctions/dma.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/dma.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/dma.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/localization/localization.o: src/localization/localization.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/localization" 
	@${RM} ${OBJECTDIR}/src/localization/localization.o.d 
	@${RM} ${OBJECTDIR}/src/localization/localization.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/localization/localization.c  -o ${OBJECTDIR}/src/localization/localization.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/localization/localization.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/localization/localization.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/telemetry/telemetry.o: src/telemetry/telemetry.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/telemetry" 
	@${RM} ${OBJECTDIR}/src/telemetry/telemetry.o.d 
	@${RM} ${OBJECTDIR}/src/telemetry/telemetry.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/telemetry/telemetry.c  -o ${OBJECTDIR}/src/telemetry/telemetry.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/telemetry/telemetry.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/telemetry/telemetry.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/controller/controller.o: src/controller/controller.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/controller" 
	@${RM} ${OBJECTDIR}/src/controller/controller.o.d 
	@${RM} ${OBJECTDIR}/src/controller/controller.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/controller/controller.c  -o ${OBJECTDIR}/src/controller/controller.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/controller/controller.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/controller/controller.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/sensors.o: src/hardwareFunctions/sensors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/sensors.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/sensors.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/sensors.c  -o ${OBJECTDIR}/src/hardwareFunctions/sensors.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/sensors.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/sensors.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/mapping/mapping.o: src/mapping/mapping.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/mapping" 
	@${RM} ${OBJECTDIR}/src/mapping/mapping.o.d 
	@${RM} ${OBJECTDIR}/src/mapping/mapping.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/mapping/mapping.c  -o ${OBJECTDIR}/src/mapping/mapping.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/mapping/mapping.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/mapping/mapping.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
${OBJECTDIR}/src/hardwareFunctions/uart.o: src/hardwareFunctions/uart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/src/hardwareFunctions" 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/uart.o.d 
	@${RM} ${OBJECTDIR}/src/hardwareFunctions/uart.o 
	${MP_CC} $(MP_EXTRA_CC_PRE)  src/hardwareFunctions/uart.c  -o ${OBJECTDIR}/src/hardwareFunctions/uart.o  -c -mcpu=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/src/hardwareFunctions/uart.o.d"        -g -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -O0 -msmart-io=1 -Wall -msfr-warn=off  
	@${FIXDEPS} "${OBJECTDIR}/src/hardwareFunctions/uart.o.d" $(SILENT)  -rsi ${MP_CC_DIR}../ 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemblePreproc
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/micromouse.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/micromouse.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -D__DEBUG -D__MPLAB_DEBUGGER_PK3=1  -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x800:0x81F -mreserve=data@0x820:0x821 -mreserve=data@0x822:0x823 -mreserve=data@0x824:0x825 -mreserve=data@0x826:0x84F   -Wl,,,--defsym=__MPLAB_BUILD=1,--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,--defsym=__MPLAB_DEBUGGER_PK3=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/micromouse.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -o dist/${CND_CONF}/${IMAGE_TYPE}/micromouse.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}      -mcpu=$(MP_PROCESSOR_OPTION)        -omf=elf -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -Wl,,,--defsym=__MPLAB_BUILD=1,$(MP_LINKER_FILE_OPTION),--stack=16,--check-sections,--data-init,--pack-data,--handles,--isr,--no-gc-sections,--fill-upper=0,--stackguard=16,--no-force-link,--smart-io,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--report-mem,--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml$(MP_EXTRA_LD_POST) 
	${MP_CC_DIR}/xc16-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/micromouse.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} -a  -omf=elf  
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
