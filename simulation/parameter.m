%Mechanical structure
r_axis = 0.05 %in m
r_wheel = 0.02 %in m
mass = 0.2 %in kg

%Motor characteristics acc. to datasheet Faulhaber motor
R = 8 %Resistance in Ohm
L = 420e-6 %Inductance in H
k_T = 0.0084 %Torque constant in Nm/A
b = 167E-9 % will later be calculated by (k_T * leerlaufstrom) / leerlaufdrehzahl
T_M = 0.135 %Motor time constant
K_M = 117 %Motor P parameter
max_motor_voltage = 6
K_G = 1/33  %gear rate
%Intertia
J_motor = 68e-9 %in kg*m^2 acc. to datasheet Faulhaber motor
J_mass = mass * r_wheel^2
J = (J_motor + (J_mass/2)) * K_G %mass is shared over the motors

%Controller Parameter
%according to on paper equations
multi = 1 %0.01 is good
P_y = 1 / (4 * T_M * K_G * r_wheel * K_M) %critically damped
P_x = P_y * 0.05 * multi %before 4
P_theta = 1 / (4 * 2 * T_M * K_G * r_wheel * (1/r_axis) * K_M) %critically damped
x_start = 0.02
y_goal = 0.2
theta_start = 100 %degree w.r.t. x-axis

start_time = 0
end_time = 3

trajectory_generation_visualization
P_y
%Start Simulation
sim('mouse_model', [start_time end_time]);