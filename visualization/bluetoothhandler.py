# On Linux:
# sudo apt-get install libbluetooth-dev
# sudo apt-get install python-dev

import bluetooth




class BluetoothHandler(object):

    def __init__(self):
        """Initialize the Bluetooth socket."""
        self.socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        self.device = []

    def connect_to_device(self, mac_adress):
        """Connect to the device with the given MAC adress."""
        try:
            self.socket.connect((mac_adress, 1))
            self.device = mac_adress
            print("Connected to ", mac_adress)
        except bluetooth.BluetoothError:
            raise

        return None

    def disconnect(self):
        """Close the Bluetooth socket."""
        if self.device:
            try:
                self.socket.close()
                print("Disconnected from ", self.device)
            except bluetooth.BluetoothError:
                raise

        return None

    def read_data(self):
        """
        Read data from the bluetooth socket byte per byte and return
        the full string when EOT is encountered. EOT is not included in
        the output string.
        """

        data = []

        if self.device:
            try:
                while True:
                    data.append(self.socket.recv(1024))

                    # Check for EOT
                    if data[-1] == 4:
                        break

            except IOError: raise

        return data[:-1]

    def send_data(self, string):
        self.socket.send(string)