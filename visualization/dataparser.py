



class DataParser(object):

    @staticmethod
    def parse(string):

        parse_methods = {'c': parse_control_message,
                         't': parse_test_method,
                         }

        # Call the correct parse function based on the first character of the string
        parse_methods[string[0]](string[1:])



def parse_control_message(string):
    return string


def parse_test_method(string):
    return string