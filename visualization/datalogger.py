import numpy as np
import scipy.io as io
import datetime

class DataLogger(object):

    def __init__(self):
        self.data = {}
        self.data['time'] = []

    def append(self, timestamp, new_data):
        """ Append new data with timestamp to the logging history"""
        for key in new_data:
            if not key in self.data:
                self.data[key] = []

            if len(self.data[key]) < len(self.data['time']):
                self.data[key].extend([float('NaN')] * (len(self.data['time']) - len(self.data[key])))

            self.data[key].append(new_data[key])

        self.data['time'].append(timestamp)


    def save(self, savepath='./logging/'):
        """ Save the logging history to a file. Standard path is ./logging/"""

        n_timestamps = len(self.data['time'])

        # Convert lists to numpy arrays to save them to .mat-file
        for key in self.data:
            if len(self.data[key]) < n_timestamps:
                self.data[key].extend([float('NaN')] * (n_timestamps - len(self.data[key])))

            if len(self.data[key]) > n_timestamps:
                del self.data[key][n_timestamps - len(self.data[key]) : ]

            self.data[key] = np.array(self.data[key])


        io.savemat(savepath + datetime.datetime.now().strftime("%Y%m%d%H%M%S"), self.data)
